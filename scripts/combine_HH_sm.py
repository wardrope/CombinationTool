# ATLAS combination of HH projections

# Load the shared library. To compile it, follow the instructions given in the
# README.
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem

try:
    gSystem.Load("lib/libCombinationTool.so")
except:
    print ("Could not load library. Make sure that it was compiled correctly.")


# Define
bbbb = ROOT.Measurement("bbbb")
bbbb.SetFileName("../InputFiles/bbbb/sm_lhh01_resolved_4b_NoSyst_jetPtMin40GeV_injectedMu0_ExtrapolatedLumi3000fb_combined_hh4b_model.root")
bbbb.SetWorkspaceName("combined")
bbbb.SetModelConfigName("ModelConfig")
bbbb.SetSnapshotName("NominalParamValues")
bbbb.SetDataName("obsData")

bbtt = ROOT.Measurement("bbtt")
bbtt_file = "../InputFiles/bbtautau/ApprovedRWbeff_TauTau_3000.July01TT3000_HH_13T"
##bbtt_file += "eV_July01TT3000_StatOnly_tautau_SMRW_BDT_0/combined/0.root"
bbtt_file += "eV_July01TT3000_Systs_tautau_SMRW_BDT_0/combined/0.root"
bbtt.SetFileName(bbtt_file)
bbtt.SetWorkspaceName("combined")
bbtt.SetModelConfigName("ModelConfig")
bbtt.SetSnapshotName("NominalParamValues")
bbtt.SetDataName("obsData")

bbyy = ROOT.Measurement("bbyy")
bbyy_file = "../InputFiles/MW_bbyy/HH01_combined_HHmass_model.root"
bbyy.SetFileName(bbyy_file)
bbyy.SetWorkspaceName("combined")
bbyy.SetModelConfigName("ModelConfig")
bbyy.SetSnapshotName("NominalParamValues")
bbyy.SetDataName("obsData")
#bbyy = ROOT.Measurement("bbyy")
##bbyy_file = "InputFiles/bbgammagamma/yybb_ws.root"
#bbyy.SetFileName(bbyy_file)
#bbyy.SetWorkspaceName("wsHHyybb")
#bbyy.SetModelConfigName("mconfig")
##bbtt.SetSnapshotName("NominalParamValues")
#bbyy.SetDataName("obsData")
#combined.AddMeasurement(bbyy)

correlation = ROOT.CorrelationScheme("CorrelationScheme")
#correlation.SetParametersOfInterest("SignalStrength")
correlation.CorrelateParameter("bbyy::SigXsecOverSM,bbtt::SigXsecOverSM,bbbb::SigXsecOverSM", "mu[1.0, 0., 5.]") 
#correlation.CorrelateParameter("bbyy::SigXsecOverSM,bbbb::NormFactorToPrediction", "mu[1.0, 0., 5.]") 
correlation.SetParametersOfInterest("mu")
#correlation.CorrelateParameter("bbyy::Lumi,bbbb::alpha_2016_Luminosity,bbbb::Lumi", "Lumi")
#correlation.CorrelateParameter("bbyy::Lumi,bbbb::alpha_2016_Luminosity", "Luminosity")

# Use the correlation scheme for the combined measurement.
combined = ROOT.CombinedMeasurement("combined_master", "hyper_combined", "ModelConfig", "obsData")
#CombinedMeasurement( std::string CombinedMeasurementName = "master", std::string WorkspaceName = "combined", std::string ModelConfigName = "ModelConfig", std::string DataName = "combData" );
combined.SetCorrelationScheme(correlation)
#combined.AddMeasurement(bbbb)
combined.AddMeasurement(bbyy)
combined.AddMeasurement(bbtt)

combined.CollectMeasurements()
combined.CombineMeasurements()

combined.writeToFile("../CombinedWorkspaces/FromCombinationTool/test_bbyy_bbtt.root")
# Add common signal strength parameter for all signal samples
#signalstrength = ROOT.ParametrisationScheme("signal_strength")
#signalstrength.AddExpression("SignalStrength[1.0,0.0,10.0]")
#
#signalstrength.AddExpression("mu_XS7_ggF=expr::mu_XS7_ggF_prime('@0*@1',mu,mu_XS7_ggF)")
#signalstrength.AddExpression("mu_XS8_ggF=expr::mu_XS8_ggF_prime('@0*@1',mu,mu_XS8_ggF)")
#signalstrength.AddExpression("mu_XS7_VBF=expr::mu_XS7_VBF_prime('@0*@1',mu,mu_XS7_VBF)")
#signalstrength.AddExpression("mu_XS8_VBF=expr::mu_XS8_VBF_prime('@0*@1',mu,mu_XS8_VBF)")
#signalstrength.AddExpression("mu_XS7_WH=expr::mu_XS7_WH_prime('@0*@1',mu,mu_XS7_WH)")
#signalstrength.AddExpression("mu_XS8_WH=expr::mu_XS8_WH_prime('@0*@1',mu,mu_XS8_WH)")
#signalstrength.AddExpression("mu_XS7_ZH=expr::mu_XS7_ZH_prime('@0*@1',mu,mu_XS7_ZH)")
#signalstrength.AddExpression("mu_XS8_ZH=expr::mu_XS8_ZH_prime('@0*@1',mu,mu_XS8_ZH)")
#
## Combine the different parametrisations
#parametrisation = ROOT.ParametrisationSequence("parametrisation")
#parametrisation.AddScheme(corrections)
#parametrisation.AddScheme(signalstrength)
#parametrisation.SetParametersOfInterest("SignalStrength")
#parametrisation.SetParametersOfInterest("mu,mu_XS7_ggF,mu_XS8_ggF,mu_XS7_VBF,mu_XS8_VBF,mu_XS7_WH,mu_XS8_WH,mu_XS7_ZH,mu_XS8_ZH,mu_BR_WW,mu_BR_tautau")
#
## Carry out the re-parametrisation
#combined.SetParametrisationSequence(parametrisation)
#combined.ParametriseMeasurements()
#combined.MakeSnapshots(ROOT.CombinedMeasurement.nominal, True);

# Generate Asimov data (NP measured in unconditional fit, generated for mu = 1)
#combined.MakeAsimovData(ROOT.kTRUE, ROOT.CombinedMeasurement.ucmles, ROOT.CombinedMeasurement.nominal)
#combined.MakeSnapshots(ROOT.CombinedMeasurement.ucmles, ROOT.kTRUE)

# Save the combined workspace
#combined.writeToFile("CombinedWorkspaces/FromCombinationTool/test_parameterised.root")
combined.Print()
