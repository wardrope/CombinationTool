# ATLAS combination of HH projections

# Load the shared library. To compile it, follow the instructions given in the
# README.
import argparse
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem

try:
    gSystem.Load("lib/libCombinationTool.so")
except:
    print ("Could not load library. Make sure that it was compiled correctly.")

def main():
    parser = argparse.ArgumentParser(description="Combine HH measurements")
    parser.add_argument("channels", metavar='channels', type=str, nargs="+",#default="Processed_EventAnaResults_combined.root",
                        help="Give the list of channels to be combined")
    parser.add_argument("--systs", metavar='-s', type=str, default="NoSyst",
                        help="Give the list of channels to be combined")
    parser.add_argument("--lhh", type=int, nargs="+",
                        help="lambda_{HHH}/lambda_{HHH}^{SM} value(s) to process")
    parser.add_argument("--injSigMu", type=int, default=0,
                        help="injected signal strength")
    args = parser.parse_args()
    #for channel in args.channels:
    print(args.channels)
    print(args.systs)
    print(args.lhh)
    print(args.injSigMu)
    
    lambdas_HHH = []
    if not args.lhh:
        lambdas_HHH = [ format_lambda_HHH(i) for i in range(-20, 21)]
    else:
        for lhh in args.lhh:
            lambdas_HHH.append(format_lambda_HHH(lhh))
    
    for lhhh in lambdas_HHH:
        make_combined_workspace(args.systs, lhhh, args.channels, args.injSigMu)

def make_combined_workspace(syst, lHHH, channels, injected_mu):
    # Use the correlation scheme for the combined measurement.
    combined = ROOT.CombinedMeasurement("combined_master", "combined", "ModelConfig", "obsData")
    #CombinedMeasurement( std::string CombinedMeasurementName = "master", std::string WorkspaceName = "combined", std::string ModelConfigName = "ModelConfig", std::string DataName = "combData" );
    if "bbbb" in channels:
        syst_string = syst 
        if syst == "AllSyst":
            syst_string = "BackSyst"
        bbbb = ROOT.Measurement("bbbb")
        bbbb.SetFileName("../InputFiles/bbbb/sm_lhh"+lHHH
                            +"_resolved_4b_"+syst_string
                            +"_jetPtMin40GeV_injectedMu"
                            +str(injected_mu)+"_ExtrapolatedLumi3000fb"
                            +"_combined_hh4b_model.root"
                        )
        bbbb.SetWorkspaceName("combined")
        bbbb.SetModelConfigName("ModelConfig")
        bbbb.SetSnapshotName("NominalParamValues")
        bbbb.SetDataName("asimovData")
        #bbbb.SetDataName("obsData")
        combined.AddMeasurement(bbbb)

    if "bbtt" in channels:
        bbtt = ROOT.Measurement("bbtt")
#ApprovedRWbeff_TauTau_3000.FinalFullSystNoMCStatsTT3000_HH_13TeV_FinalFullSystNoMCStatsTT3000_Systs_tautau_SML20RW_BDT_0
#ApprovedRWbeff_TauTau_3000.FinalFullSystNoMCStatsTT3000_HH_13TeV_FinalFullSystNoMCStatsTT3000_Systs_tautau_SMLm01RW_BDT_0
        syst_string = "StatOnly"
        if syst == "AllSyst":
            #syst_string = "FullSyst"
            syst_string = "FullSystNoMCStats"
        lHHH_string = ("SML"+lHHH+"RW").replace('-','m')
        
        bbtt.SetFileName(
                         "../InputFiles/bbtautau/LambdaNOSMInjection/"
                         +"ApprovedRWbeff_TauTau_3000.Final" + syst_string
                         +"TT3000_HH_13TeV_Final" + syst_string +"TT3000_Systs"
                         +"_tautau_"+lHHH_string+"_BDT_0/"
                         +"combined/0.root"
                        )
        bbtt.SetWorkspaceName("combined")
        bbtt.SetModelConfigName("ModelConfig")
        bbtt.SetSnapshotName("NominalParamValues")
        bbtt.SetDataName("asimovData")
        #bbtt.SetDataName("obsData")
        combined.AddMeasurement(bbtt)

    if "bbyy" in channels:
        syst_string = syst 
        if syst == "AllSyst":
            syst_string = "BackSyst"
        bbyy = ROOT.Measurement("bbyy")
        bbyy.SetFileName(
                         "../InputFiles/bbyy/HH"+lHHH+"_injectedMu"
                         +str(injected_mu)
                         +"_"+syst_string+"_seed11_combined_HHmass_model.root"
                        )
        bbyy.SetWorkspaceName("combined")
        bbyy.SetModelConfigName("ModelConfig")
        bbyy.SetSnapshotName("NominalParamValues")
        #bbyy.SetDataName("obsData")
        bbyy.SetDataName("asimovData")
        combined.AddMeasurement(bbyy)
#
#
#    pois = {
#                "bbyy":"bbyy::SigXsecOverSM",
#                "bbtt":"bbtt::SigXsecOverSM",
#                "bbbb":"bbbb::SigXsecOverSM",
#            }
    pois = {
                "bbyy":"mu_bbyy_linear",
                "bbtt":"mu_bbtt_linear",
                "bbbb":"mu_bbbb_linear",
            }
    correlation = ROOT.CorrelationScheme("CorrelationScheme")
    pois_to_corr = [pois[chan] for chan in channels]
    #pois_to_corr.append("mu")
    np_lumi = {
                "bbyy":"bbyy::Lumi",
                "bbbb":"bbbb::Lumi",
                "bbtt":"bbtt::Lumi",
                #"bbtt":"bbtt::alpha_ATLAS_LUMI_2015_2016",
               }
    np_lumi_to_corr = [np_lumi[chan] for chan in channels]
    correlation.CorrelateParameter(','.join(np_lumi_to_corr), "Luminosity[1.0, 0.99, 1.01]")
    #correlation.CorrelateParameter("bbtt::ATLAS_LUMI_2015_2016,bbyy::Lumi", "OtherLumi")
    #correlation.SetParametersOfInterest("mu")
#    correlation.SetParametersOfInterest("mu,bbyy::SigXsecOverSM,bbtt::SigXsecOverSM,bbbb::SigXsecOverSM")
    correlation.RenameParameter("bbbb", "SigXsecOverSM", "mu_bbbb_linear[1.0]")
    correlation.RenameParameter("bbtt", "SigXsecOverSM", "mu_bbtt_linear[1.0]")
    correlation.RenameParameter("bbyy", "SigXsecOverSM", "mu_bbyy_linear[1.0]")
   # correlation.SetParametersOfInterest("mu_bbbb_linear,mu_bbyy_linear,mu_bbtt_linear")
    correlation.SetParametersOfInterest(','.join(pois_to_corr))
    combined.SetCorrelationScheme(correlation)
    #combined.SetPrunedNuisanceParameters("Lumi")
    combined.CollectMeasurements()
    combined.CombineMeasurements()
#
    corrections = ROOT.ParametrisationScheme("corrections")
    signalstrength = ROOT.ParametrisationScheme("signalstrength")
    signalstrength.AddExpression("mu[1.0,0.0,10.0]")
#Parametrise μ in CombinationTool. Global μ = 0.9019μbbττ = 0.9268μbbbb =  0.9239μbbγγ.
    if "bbbb" in channels:
        corrections.AddExpression("mu_bbbb_linear=expr::mu_bbbb_scaling('0.9268*@0',mu_bbbb[1.0])")
        signalstrength.AddExpression("mu_bbbb=expr::mu_bbbb_prime('@0*@1',mu,mu_bbbb)")
    if "bbtt" in channels:
        corrections.AddExpression("mu_bbtt_linear=expr::mu_bbtt_scaling('0.9019*@0',mu_bbtt[1.0])")
        signalstrength.AddExpression("mu_bbtt=expr::mu_bbtt_prime('@0*@1',mu,mu_bbtt)")
    if "bbyy" in channels:
        corrections.AddExpression("mu_bbyy_linear=expr::mu_bbyy_scaling('0.9239*@0',mu_bbyy[1.0])")
        signalstrength.AddExpression("mu_bbyy=expr::mu_bbyy_prime('@0*@1',mu,mu_bbyy)")
    parametrisation = ROOT.ParametrisationSequence("parametrisation")
    parametrisation.AddScheme(corrections)
    parametrisation.AddScheme(signalstrength)
    #parametrisation.SetParametersOfInterest(','.join(pois_to_corr))
    parametrisation.SetParametersOfInterest("mu,mu_bbbb,mu_bbyy,mu_bbtt")
    combined.SetParametrisationSequence(parametrisation)
    combined.ParametriseMeasurements()
    combined.writeToFile("../CombinedWorkspaces/FromCombinationTool/"
                         +"_".join(channels+["lHHH"+lHHH,syst,"injectedMu"+str(injected_mu)])+".root")
#    # Add common signal strength parameter for all signal samples
    #signalstrength = ROOT.ParametrisationScheme("signal_strength")
#    #signalstrength.AddExpression("SignalStrength[1.0,0.0,10.0]")
#    #
#    #signalstrength.AddExpression("mu_XS7_ggF=expr::mu_XS7_ggF_prime('@0*@1',mu,mu_XS7_ggF)")
#    #signalstrength.AddExpression("mu_XS8_ggF=expr::mu_XS8_ggF_prime('@0*@1',mu,mu_XS8_ggF)")
#    #signalstrength.AddExpression("mu_XS7_VBF=expr::mu_XS7_VBF_prime('@0*@1',mu,mu_XS7_VBF)")
#    #signalstrength.AddExpression("mu_XS8_VBF=expr::mu_XS8_VBF_prime('@0*@1',mu,mu_XS8_VBF)")
#    #signalstrength.AddExpression("mu_XS7_WH=expr::mu_XS7_WH_prime('@0*@1',mu,mu_XS7_WH)")
#    #signalstrength.AddExpression("mu_XS8_WH=expr::mu_XS8_WH_prime('@0*@1',mu,mu_XS8_WH)")
#    #signalstrength.AddExpression("mu_XS7_ZH=expr::mu_XS7_ZH_prime('@0*@1',mu,mu_XS7_ZH)")
#    #signalstrength.AddExpression("mu_XS8_ZH=expr::mu_XS8_ZH_prime('@0*@1',mu,mu_XS8_ZH)")
#    #
#    ## Combine the different parametrisations
#    #parametrisation = ROOT.ParametrisationSequence("parametrisation")
#    #parametrisation.AddScheme(corrections)
#    #parametrisation.AddScheme(signalstrength)
#    #parametrisation.SetParametersOfInterest("SignalStrength")
#    #parametrisation.SetParametersOfInterest("mu,mu_XS7_ggF,mu_XS8_ggF,mu_XS7_VBF,mu_XS8_VBF,mu_XS7_WH,mu_XS8_WH,mu_XS7_ZH,mu_XS8_ZH,mu_BR_WW,mu_BR_tautau")
#    #
#    ## Carry out the re-parametrisation
#    #combined.SetParametrisationSequence(parametrisation)
#    #combined.ParametriseMeasurements()
#    #combined.MakeSnapshots(ROOT.CombinedMeasurement.nominal, True);
#
    # Generate Asimov data (NP measured in unconditional fit, generated for mu = 1)
#    combined.MakeAsimovData(ROOT.kTRUE, ROOT.CombinedMeasurement.ucmles, ROOT.CombinedMeasurement.nominal)
#    combined.MakeSnapshots(ROOT.CombinedMeasurement.ucmles, ROOT.kTRUE)
#
#    # Save the combined workspace
#    combined.writeToFile("../CombinedWorkspaces/FromCombinationTool/"
#                         +"_".join(channels+["lHHH"+lHHH,syst,"injectedMu"+str(injected_mu),"withAsimov"])+".root")
    #combined.writeToFile("CombinedWorkspaces/FromCombinationTool/test_parameterised.root")
    combined.Print()

def format_lambda_HHH(lambda_HHH):
    lambdaVal = int(lambda_HHH)
    if abs(lambdaVal) < 10:
        if lambdaVal < 0:
            lambda_HHH = "-0"+str(abs(lambdaVal))
        else:
            lambda_HHH = "0"+str(lambda_HHH)
    else:
        lambda_HHH = str(lambda_HHH)
    return lambda_HHH

if __name__ == '__main__':
    main()
