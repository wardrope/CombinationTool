#
# This module is used to find ROOT for the build of the analysis.
#

# First try to find ROOT using the CMake code bundled with ROOT itself:
set( _pathBackup ${CMAKE_MODULE_PATH} )
set( CMAKE_MODULE_PATH )
find_package( ROOT QUIET )

# If it was found like that, yay!
if( ROOT_FOUND )
   if( NOT ROOT_FOUND_PRINTED )
      message( STATUS "Found ROOT: ${ROOT_BINARY_DIR}/root-config "
	 "(found version \"${ROOT_VERSION}\")" )
      set( ROOT_FOUND_PRINTED TRUE
	 CACHE INTERNAL "Variable showing that the info line was printed" )
   endif()
else()

   # Look for root-config. Everything else is found using this script:
   find_program( ROOT_CONFIG_EXECUTABLE root-config
      HINTS $ENV{ROOTSYS}/bin )
   mark_as_advanced( ROOT_CONFIG_EXECUTABLE )

   # Get all the necessary variables:
   execute_process(
      COMMAND ${ROOT_CONFIG_EXECUTABLE} --prefix
      OUTPUT_VARIABLE ROOTSYS
      OUTPUT_STRIP_TRAILING_WHITESPACE )

   execute_process(
      COMMAND ${ROOT_CONFIG_EXECUTABLE} --version
      OUTPUT_VARIABLE ROOT_VERSION
      OUTPUT_STRIP_TRAILING_WHITESPACE )

   execute_process(
      COMMAND ${ROOT_CONFIG_EXECUTABLE} --incdir
      OUTPUT_VARIABLE ROOT_INCLUDE_DIR
      OUTPUT_STRIP_TRAILING_WHITESPACE )
   set( ROOT_INCLUDE_DIRS ${ROOT_INCLUDE_DIR} )

   execute_process(
      COMMAND ${ROOT_CONFIG_EXECUTABLE} --libdir
      OUTPUT_VARIABLE ROOT_LIBRARY_DIR
      OUTPUT_STRIP_TRAILING_WHITESPACE )
   set( ROOT_LIBRARY_DIRS ${ROOT_LIBRARY_DIR} )

   # Look for all the libraries (possily) used by the analysis code:
   set( rootlibs Core RIO Net Hist Graf Graf3d Gpad Tree Postscript
     Matrix Physics MathCore Eve RGL )
   foreach( _cpt ${rootlibs} )
      find_library( ROOT_${_cpt}_LIBRARY ${_cpt} PATHS ${ROOT_LIBRARY_DIR} )
      if( ROOT_${_cpt}_LIBRARY AND NOT TARGET ${_cpt} )
        mark_as_advanced( ROOT_${_cpt}_LIBRARY )
	# Set up an imported library for it:
	get_filename_component( _cptName ${_cpt} NAME )
        add_library( ${_cpt} SHARED IMPORTED )
	set_target_properties( ${_cpt} PROPERTIES
	  IMPORTED_LOCATION ${ROOT_${_cpt}_LIBRARY}
	  IMPORTED_SONAME ${_cptName} )
      endif()
   endforeach()
   unset( rootlibs )
   unset( _cpt )
   unset( _cptName )

   # Look for executables (possibly) used by the analysis code:
   set( roottools rootcint )
   foreach( _cpt ${roottools} )
     find_program( ROOT_${_cpt}_CMD ${_cpt} PATHS ${ROOT_BINARY_DIR} )
     if( ROOT_${_cpt}_CMT )
       mark_as_advanced( ROOT_${_cpt}_CMD )
     endif()
   endforeach()

   # Construct the compiler flags:
   execute_process(
      COMMAND ${ROOT_CONFIG_EXECUTABLE} --cflags
      OUTPUT_VARIABLE __cflags
      OUTPUT_STRIP_TRAILING_WHITESPACE )
   string( REGEX MATCHALL "-(D|U)[^ ]*" ROOT_DEFINITIONS "${__cflags}" )
   string( REGEX REPLACE "(^|[ ]*)-I[^ ]*" "" ROOT_CXX_FLAGS "${__cflags}" )
   string( REGEX REPLACE "(^|[ ]*)-I[^ ]*" "" ROOT_C_FLAGS "${__cflags}" )

   # Handle the standard find_package arguments:
   include( FindPackageHandleStandardArgs )
   find_package_handle_standard_args( ROOT FOUND_VAR ROOT_FOUND
      REQUIRED_VARS ROOT_CONFIG_EXECUTABLE ROOTSYS ROOT_INCLUDE_DIR
                    ROOT_LIBRARY_DIR ROOT_Core_LIBRARY
      VERSION_VAR ROOT_VERSION )

endif()

# Restore the path:
set( CMAKE_MODULE_PATH ${_pathBackup} )
unset( _pathBackup )
