# Makefile to compile CombinationTool into a shared library
#
# Description
#   Compiles CombinationTool into a shared library to be used from Python, etc.
#   on various systems. The tool is based on ROOT, RooFit and RooStats. To setup
#   the environment, follow the instructions given in the README.
#
# Instructions to compile the library
#   1. Make sure that $(ROOTSYS) is set correctly. The easiest way is
#      source bin/thisroot.sh
#      in your ROOT base directory. Alternatively CVMFS can be used to setup ROOT.
#   2. Run make to build the shared library in lib.

MAKEFLAGS = --no-print-directory -r -s --pedantic -Wall

# Use Makefile.arch from ROOT
ifeq ($(ROOTSYS),)
$(error $$ROOTSYS is not defined)
endif
ROOT_MAKEFILE = /usr/local/Cellar/root/6.12.04_3/etc/root/Makefile.arch
#ROOT_MAKEFILE = /usr/local/Cellar/root/6.14.00_2/etc/root/Makefile.arch
#ROOT_MAKEFILE = $(firstword $(wildcard $(ROOTSYS)/etc/Makefile.arch $(ROOTSYS)/test/Makefile.arch))
ifeq ($(ROOT_MAKEFILE),)
$(error Could not find Makefile.arch under $(ROOTSYS)/etc or $(ROOTSYS)/test)
else
-include $(ROOT_MAKEFILE)
endif
ifeq ($(RC),)
ifeq ($(ROOTCONFIG),)
$(error $(ROOT_MAKEFILE) did not define $$RC or $$ROOTCONFIG)
else
RC = $(ROOTCONFIG)
endif
endif

# Configuration
PACKAGE          = CombinationTool
LD_LIBRARY_PATH :=.:./lib:$(ROOTSYS)/lib:$(LD_LIBRARY_PATH)
OBJDIR           = obj
DEPDIR           = $(OBJDIR)/dep
VPATH            = $(OBJDIR)
INCLUDES         = -I./inc -I$(ROOTSYS)/include

CINTFLAGS        =
DICTHEAD         = $(PACKAGE)_Dict.h
DICTFILE         = $(PACKAGE)_Dict.C
DICTOBJ          = $(PACKAGE)_Dict.o
DICTLDEF         = inc/LinkDef1.h
SKIPCPPLIST      =
SKIPHLIST        =  $(DICTHEAD) $(DICTLDEF)
LIBFILE          = ./lib/lib$(PACKAGE).a
SHLIBFILE        = ./lib/lib$(PACKAGE).so
DLLIBFILE        = ./lib/lib$(PACKAGE).dll
HEADERDEST       = ./include/
CXXFLAGS        += -std=c++11

UNAME            = $(shell uname)

INSTALLED_LIBS   = $(patsubst $(1)/lib%.$(DllSuf),-l%,$(wildcard $(patsubst %,$(1)/lib%.$(DllSuf),$(2))))
ROOTLIBDIR      := $(shell $(RC) --libdir)
ROOTLIBS        += $(call INSTALLED_LIBS,$(ROOTLIBDIR),HistFactory RooStats RooFit RooFitCore Thread Minuit Foam TreePlayer MathMore Html)
LINKLIBS         = $(ROOTLIBS) -lCombinationTool

default: shlib

# List of all source files to build
HLIST     = $(filter-out $(SKIPHLIST),$(wildcard inc/*.h))
CPPLIST   = $(filter-out $(SKIPCPPLIST),$(patsubst src/%,%,$(wildcard src/*.$(SrcSuf))))
DICTHLIST = $(HLIST)

# List of all object files to build
OLIST     = $(patsubst %.cxx,%.o,$(CPPLIST))
OLIST     = $(CPPLIST:.$(SrcSuf)=.o)

# Executable programs, ending with .cpp by convention
BINDIR    = ./bin
BinSuf    = .cpp
BINLIST   = $(basename $(wildcard *$(BinSuf)))

# Implicit rule to compile all classes
%.o : src/%.cxx
	@echo "Compiling $<"
	@mkdir -p $(OBJDIR)
	#@echo $(CXX) $(CXXFLAGS) -g -c $< -o $(OBJDIR)/$(notdir $@) $(INCLUDES)
	@$(CXX) $(CXXFLAGS) -g -c $< -o $(OBJDIR)/$(notdir $@) $(INCLUDES)

# Rule to make the dictionary
$(DICTFILE):  $(DICTHLIST) $(DICTLDEF)
	@echo "Generating dictionary $@"
	$(ROOTSYS)/bin/rootcint -f $(DICTFILE) -c $(INCLUDES) $(CINTFLAGS) $^

$(OBJDIR)/$(DICTOBJ): $(DICTFILE)
	@echo "Compiling $<"
	@mkdir -p $(OBJDIR)
	@$(CXX) $(CXXFLAGS) -g -c $(INCLUDES) -o $@ $<

##############################
# The dependencies section
# - the purpose of the .d files is to keep track of the
#   header file dependence
# - this can be achieved using the makedepend command
##############################
# .d tries to pre-process .cc
ifneq ($(MAKECMDGOALS),clean)
ifneq ($(MAKECMDGOALS),distclean)
-include $(foreach var,$(CPPLIST:.$(SrcSuf)=.d),$(DEPDIR)/$(var)) /dev/null
endif
endif

$(DEPDIR)/%.d: %.$(SrcSuf)
	@mkdir -p $(DEPDIR)
	if test -f $< ; then \
		echo "Making $(@F)"; \
		$(SHELL) -ec '$(CPP) -MM $(CXXFLAGS) $(INCLUDES) $< | sed '\''/Cstd\/rw/d'\'' > $@'; \
	fi

# Rule to combine objects into a library
$(LIBFILE): $(OLIST) $(OBJDIR)/$(DICTOBJ)
	@echo "Making static library: $(LIBFILE)"
	mkdir -p lib
	@rm -f $(LIBFILE)
	@ar q $(LIBFILE) $(addprefix $(OBJDIR)/,$(OLIST)) $(OBJDIR)/$(DICTOBJ)
	@ranlib $(LIBFILE)

# Rule to combine objects into a unix shared library
$(SHLIBFILE): $(OLIST) $(OBJDIR)/$(DICTOBJ)
	@echo "Making shared library: $(SHLIBFILE)"
	mkdir -p lib
	@rm -f $(SHLIBFILE)
	@$(LD) $(CXXFLAGS) $(SOFLAGS) $(addprefix $(OBJDIR)/,$(OLIST)) $(OBJDIR)/$(DICTOBJ) -o $(SHLIBFILE) $(ROOTLIBS)

# Rule to combine objects into a windows shared library
$(DLLIBFILE): $(OLIST) $(OBJDIR)/$(DICTOBJ)
	@echo "Making dll file: $(DLLIBFILE)"
	mkdir -p lib
	@rm -f $(DLLIBFILE)
	$(LD) -Wl,--export-all-symbols -Wl,--export-dynamic -Wl,--enable-auto-import -Wl,-Bdynamic -shared --enable-auto-image-base -Wl,-soname -o $(DLLIBFILE) -Wl,--whole-archive $(addprefix $(OBJDIR)/,$(OLIST) $(patsubst %.$(SrcSuf),%.o,$(DICTFILE))) -Wl,--no-whole-archive -L$(ROOTSYS)/lib -lCore -lCint -lHist -lGraf -lGraf3d -lTree -lRint -lPostscript -lMatrix -lMinuit -lPhysics -lHtml -lm

includeinst:
	mkdir -p $(HEADERDEST)
	cp -f inc/*.h $(HEADERDEST)
	if [ -e $(DICTFILE) ] ; then cp -f inc/*.h $(HEADERDEST) ; fi

# Useful build targets
lib: $(LIBFILE) includeinst
shlib: $(SHLIBFILE) includeinst
winlib: $(DLLIBFILE) includeinst

clean:
	@echo "Deleting all libraries and executables..."
	rm -f $(DICTFILE) $(DICTHEAD)
	rm -f $(SHLIBFILE)
	rm -f $(OBJDIR)/*.o
	rm -f $(DEPDIR)/*.d
	rm -f $(LIBFILE)
	rm -f $(SHLIBFILE)
	rm -f $(DLLIBFILE)
	for ex in $(BINLIST); do \
		rm -f $(BINDIR)/$${ex}; \
	done

distclean:
	@echo "Deleting all libraries and executables, including ./include and ./lib"
	rm -rf obj
	rm -f *~
	rm -f *_Dict*
	rm -f $(SHLIBFILE)
	rm -f $(LIBFILE)
	rm -f $(SHLIBFILE)
	rm -f $(DLLIBFILE)
	touch ./lib/libdummy ./include/dummy.h
	rm -rf ./lib/lib* ./include/*.h
	for ex in $(BINLIST); do \
		rm -f $(BINDIR)/$${ex}; \
	done

bin:	shlib
	mkdir -p $(BINDIR)
	for ex in $(BINLIST); do \
		echo "Linking $(BINDIR)/$${ex}"; \
		$(CXX) $(CXXFLAGS) -g $(INCLUDES) $(LINKLIBS) -lCombinationTool -o $(BINDIR)/$${ex} $${ex}$(BinSuf); \
	done

.PHONY : winlib shlib lib default clean distclean includeinst bin
